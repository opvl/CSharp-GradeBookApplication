﻿using GradeBook.Enums;
using System;
using System.Linq;

namespace GradeBook.GradeBooks
{
    public class RankedGradeBook : BaseGradeBook
    {
        public RankedGradeBook(string name, bool isWeighted) : base(name, isWeighted)
        {
            Type = GradeBookType.Ranked;
        }

        public override char GetLetterGrade(double averageGrade)
        {
            // with ranked grading systems you cannot perform grading with fewer than 5 students, throw exception if this occurs
            if (Students.Count < 5)
            {
                throw new InvalidOperationException("Ranked grading requires at least 5 students.");
            }

            //how many student must be lower than in order to drop a letter grade, by default student will be 'A'
            //total number of students, 80% = setLength * 0.2 (0.2 is the decimal representatio of 20% which is the arbitrary grade boundary separators
             
            // gives 20% of the total number of students which is then rounded to nearest whole number and cast to integer
            var threshold = (int)Math.Ceiling(Students.Count * 0.2); 

            /* Sort the students in descending order by their average grade, 
             * .Select makes sure this function only uses the AverageGrade rather than the entire student object 
             * Linq syntax*/
            var grades = Students.OrderByDescending(e => e.AverageGrade).Select(e => e.AverageGrade).ToList();

            // in order for this to return true we would have to have a grade in to the top 20% of students
            if (grades[threshold - 1] <= averageGrade)
                return 'A';
            //threshold it twice as much as we'd need to drop a grade i.e 100% - (2 * 20%) = 60% = 'B'
            else if (grades[(threshold * 2) - 1] <= averageGrade)
                return 'B';
            else if (grades[(threshold * 3) - 1] <= averageGrade)
                return 'C';
            else if (grades[(threshold * 4) - 1] <= averageGrade)
                return 'D';
            else
                return 'F';
        }

        public override void CalculateStatistics()
        {
            if (Students.Count < 5)
            {
                Console.WriteLine("Ranked grading requires at least 5 students with grades in order to properly calculate a student's overall grade.");
                return;
            }
            base.CalculateStatistics();
        }

        public override void CalculateStudentStatistics(string name)
        {
            if (Students.Count < 5)
            {
                Console.WriteLine("Ranked grading requires at least 5 students with grades in order to properly calculate a student's overall grade.");
                return;
            }
            base.CalculateStudentStatistics(name);
        }
    }
}
