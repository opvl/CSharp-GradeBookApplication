﻿using GradeBook.Enums;

namespace GradeBook.GradeBooks
{
    // : ClassToInheritFrom <-- this sets our parent class that our new class inherity properties from.
    public class StandardGradeBook : BaseGradeBook
    {
        // : base(parameter) <-- this makes sure that our parent (base) class constructor is called with the given parameter
        public StandardGradeBook(string name, bool isWeighted) : base(name, isWeighted)
        {
            Type = GradeBookType.Standard;
        } 
    }
}
